STEP 1 : Create Schema 
CREATE SCHEMA schema_name

STEP 2 : Create the following Tables 

1.Product Table

CREATE TABLE `product` (
  `prodId` int NOT NULL AUTO_INCREMENT,
  `productName` varchar(45) NOT NULL,
  `prodDescription` varchar(45) DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `discount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`prodId`),
  UNIQUE KEY `prodId_UNIQUE` (`prodId`),
  UNIQUE KEY `productName_UNIQUE` (`productName`)
)


2.Cart Table

CREATE TABLE `cart` (
  `cardId` int NOT NULL AUTO_INCREMENT,
  `customerId` int DEFAULT NULL,
  `product` varchar(45) DEFAULT NULL,
  `quantity` decimal(10,0) DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `priceAfterDiscount` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`cardId`),
  UNIQUE KEY `cardId_UNIQUE` (`cardId`)
)

3.User Table

CREATE TABLE `user` (
  `userId` int NOT NULL AUTO_INCREMENT,
  `userName` varchar(45) DEFAULT NULL,
  `userMailId` varchar(45) DEFAULT NULL,
  `userRole` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `userId_UNIQUE` (`userId`),
  UNIQUE KEY `mailId_UNIQUE` (`userMailId`)
)

STEP 3 : Go to file utils -> dbConnection.py change user, password and database

STEP 4 : Run python3 day1.py

You are good to go :)