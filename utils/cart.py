from utils.dbConnection import getConnection
# display cart
def display_cart(customerId):
    connection = getConnection()
    cursor = connection.cursor()
    cursor.execute("select * from cart where customerId=%s"%customerId)
    cartItems = cursor.fetchall()
    connection.commit()
    cursor.close()
    connection.close()
    return cartItems

# add items to cart
def add_item_to_cart(customerId,productId,quantity):
    connection = getConnection()
    cursor = connection.cursor()
    cursor.execute("select * from product where prodId=%s"%productId)
    product = cursor.fetchone()
    price=float(product[3])*float(quantity)
    cursor.execute("INSERT INTO cart (customerId,product,quantity,price,priceAfterDiscount) VALUES (%s, %s, %s, %s, %s)",(customerId,product[1],quantity,price,(price)-(price*float(product[4]))))
    connection.commit()
    cursor.close()
    connection.close()
    