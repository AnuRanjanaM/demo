from utils.dbConnection import getConnection
# display products
def display_product():
    connection = getConnection()
    cursor = connection.cursor()
    cursor.execute("select * from product")
    products = cursor.fetchall()
    connection.commit()
    cursor.close()
    connection.close()
    return products

def add_product(name,desc,price,discount):
    connection = getConnection()
    cursor = connection.cursor()
    print(discount)
    disc=float(int(discount)/100)
    cursor.execute("INSERT INTO product (productName,prodDescription,price,discount) VALUES (%s, %s, %s, %s)",(name,desc,float(price),disc))
    connection.commit()
    cursor.close()

