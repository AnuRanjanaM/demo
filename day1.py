import sys
import re
import os.path
from utils.product import display_product,add_product
from utils.cart import display_cart,add_item_to_cart
from utils.dbConnection import getConnection
from utils.user import getUser
from classes.user import User

# get user details
user_email = input("Email :")
password  = input("Password :")
user = getUser(user_email)
if(user =='new'):
    name=input("Name : ")
    role=input("Role (customer/shopkeeper) : ")
    connection = getConnection()
    cursor = connection.cursor()
    cursor.execute("INSERT INTO user (userName,UserMailId,userRole) VALUES (%s, %s, %s)",(name,user_email,role))
    connection.commit()
    cursor.close()
    connection.close()
    user = User(getUser(user_email))
else:
    user = User(user)

# unauthorized
if password != 'pass':
    sys.exit("Oops unauthorized")

# TODO check user role and display menu
# TODO add classes for other classes if needed

print('\n\nWelcome ',user.name,' :)\n\n')
# display menu
def display_menu():
    print("\n**** Menu ****\n")
    print("press 1 --> Product List\n")
    print("press 2 --> Cart List\n")
    print("press 3 --> Add item to cart\n")
    print("press 4 --> Add product\n")
    print("press x --> Exit\n")

while True:
    display_menu()
    choice = input("Enter choice.... ")
    print(choice)
    if choice.lower() == 'x':
        sys.exit("Thank you :)")
    elif int(choice) == 1:
        print('\n\n***************        Product List            *****************\n\n')
        list=display_product()
        column_width=20
        if(len(list)>0):
            header=('Product Id','Product Name','Description','Price/Desc','Discount')
            for el in header:         
                print(str(el).ljust(column_width,' '),end="")
            print()
            for row in list:             
                for el in row:         
                    print(str(el).ljust(column_width,' '),end="")
                print()
        else:
            print('||                  No Product Available                          ||')
    elif int(choice) == 2:
        print('\n\n***************        Cart List            *****************\n\n')
        list=display_cart(user.id)
        column_width=20
        if(len(list)>0):
            header=('Cart Id','Customer Id','Product','Quantity','Price','Price After Discount')
            for el in header:         
                print(str(el).ljust(column_width,' '),end="")
            print()
            for row in list:             
                for el in row:         
                    print(str(el).ljust(column_width,' '),end="")
                print()
        else:
            print('||         Your Cart is Empty...Come on Shop now!!          ||')
    elif int(choice) == 3:
        list=display_product()
        if(len(list)>0):
            productId=input("Enter product id ")
            quantity=input("Enter quantity ")
            add_item_to_cart(user.id,productId,quantity)
            print('Item added to Cart!!')
        else:
            print('\n\n             No Products Available to add into cart               \n\n')
    elif int(choice) == 4:
        name=input("Product Name:")
        desc=input("Description(count/kg/ltr) : ")
        price=input("Price per Description(count/kg/ltr) : ")
        discount=input("Discount(1-100)% : ")
        add_product(name,desc,price,discount)
        print('Product Added!!')


